(function ($) {
    $.app = {
        deleteFile:function (e) {
            $(document).on('click', '.btn-delete', function (e) {
                e.preventDefault();
                var $this = $(this);
                var modal = $("#delete_modal");

                if (modal.length  > 0  && $this.length > 0)
                {
                    modal.find("form").attr('action', $this.attr('data-url'));
                    modal.modal('show');
                }
                return false;
            });
        },
        shareModal:function () {
            $(document).on('click', '.btn-show-modal', function (e) {
                e.preventDefault();
                var $this = $(this);
                var modal = $("#share_modal");

                if (modal.length  > 0  && $this.length > 0)
                {
                    modal.find("input").val($this.attr('data-url'));
                    modal.modal('show');
                }

                return false;
            });
        },
        copyLink:function () {
            $(document).on('click', '#copy_share_link', function (e) {
                e.preventDefault();
                var $this = $(this);

                if ($this.length > 0)
                {
                    $("#file_share_link").select();
                    document.execCommand("copy");
                }

                return false;
            });
        }
    };

    $(document).ready(function () {
        $.app.deleteFile();
        $.app.shareModal();
        $.app.copyLink();
    });
})(jQuery);

@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="mt-5">
            <div class="row">
                <div class="card mx-auto" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">{{$file->file_name}}</h5>
                        <p class="card-text">{{\Carbon\Carbon::parse($file->created_at)->format('M d, Y')}}</p>
                        <form  method="POST" action="{{route('file.download', $file->token)}}">
                            {!! csrf_field() !!}
                            <button class="btn btn-primary" type="submit">Download</button>
                        </form>

                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection

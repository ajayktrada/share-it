@extends('layouts.app')

@section('content')
    @include('message')
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-6">
                <div class="form-wrapper mt-5">
                    <h1 class="display-6">File Upload</h1>

                    <hr class="my-4">
                    <form method="POST" action="{{route('files.store')}}" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <input type="file" class="form-control-file" name="file" id="file">
                            @if($errors->has('file'))
                                <span class="text-danger">{{$errors->first('file')}}</span>
                            @endif
                        </div>
                        <button class="btn btn-lg btn-primary" type="submit" value="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

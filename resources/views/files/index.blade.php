@extends('layouts.app')

@section('content')
    @include('message')
    <div class="container">
        <div class="mt-5">
            <div class="row">
                <div class="offset-md-2 col-md-8">
                    <a class="btn btn-sm btn-primary mb-3" href="{{route('files.create')}}"> Upload File</a>
                </div>
                <div class="col-md-8 mx-auto">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Created At</th>
                            <th>Total Download</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($files->isNotEmpty())
                            @foreach($files as $key => $item)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$item->file_name}}</td>
                                    <td>{{\Carbon\Carbon::parse($item->created_at)->format('d, M Y')}}</td>
                                    <td>{{$item->total_download}}</td>
                                    <td>
                                        <a class="btn btn-sm btn-primary d-inline-block btn-show-modal" href="javascript:void(0);" data-url="{{route('file.show', $item->token)}}" >share</a>
                                        <a class="btn btn-sm btn-danger d-inline-block btn-delete" href="javascript:void(0);" data-url="{{route('files.destroy', $item->id)}}">Delete</a>
                                        <form method="POST" action="{{route('files.destroy', $item->id)}}">
                                            {!! method_field('delete')  !!}
                                            {!! csrf_field() !!}
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" class="text-center">No data found...</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    @include('modals.share_modal')
    @include('modals.delete_modal')
@endsection

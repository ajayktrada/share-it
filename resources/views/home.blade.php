@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center align-items-center section">
        <div class="col-6">
            <div class="jumbotron">
                <h1 class="display-4">{{ config('app.name', 'Laravel') }}</h1>
                <p class="lead">This is test application shows how easily we can upload file and sharing it with outer world.</p>
                <hr class="my-4">
                <p>In order to use this application either login or register in this demo application.</p>
                <p class="lead">
                    <a href="{{ route('login') }}" class="btn btn-lg btn-primary">{{ __('Login') }}</a>
                    <a href="{{ route('register') }}" class="btn btn-lg btn-primary">{{ __('Register') }}</a>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection

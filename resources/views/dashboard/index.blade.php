@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-6">
            <div class="jumbotron">
                <h1 class="display-4">{{config('app.name')}}</h1>
                <p class="lead">Please click on upload file button to start sharing your files.</p>
                <hr class="my-4">
                <p class="lead">
                    <a href="{{route('files.create')}}" class="btn btn-lg btn-primary">Upload file</a>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection

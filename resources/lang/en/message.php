<?php
    return [
        'file' => [
            'add' => 'File uploaded successfully.',
            'delete' => 'File deleted successfully.',
            'extension' => 'php, bmp and exe file type not supported.',
            'required' => 'The file field is required...',
            'max' => 'The file size can not be grater than 10MB.',
        ],
        'error' => 'Something goes wrong!'
    ];

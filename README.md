# Steps to setup project

#### 1] Run following commands inside the project's root directory.
```bash
- composer install
- npm install && npm run dev
```

#### 2] Provide permission to storage and bootstrap directory.

#### 3] Config your database inside ENV file and run the migration.
```bash
- php artisan db:migrate
```

#### 4] Clear config and cache of laravel.
```bash
- php artisan config:cache
```

#### 5] Using htaccess point index.php file of root directory.

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes(['verify' => true]);

Route::group(['middleware' => 'auth'], function (){

    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::resource('files', 'FilesController')->only(['index', 'create', 'store', 'destroy']);
});

Route::post('download/{file}', 'FilesController@download')->name('file.download');
Route::get('{file}', 'FilesController@show')->name('file.show');

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = [
        'file_name',
        'user_id',
        'extension',
        'path',
        'token',
        'total_download'
    ];
    
    public function getRouteKeyName()
    {
        return  'token';
    }
}

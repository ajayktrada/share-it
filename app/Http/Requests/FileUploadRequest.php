<?php

namespace App\Http\Requests;

use App\Rules\FileExtensionRule;
use Illuminate\Foundation\Http\FormRequest;

class FileUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => [
                'required',
                'max:10240', //10240 = 10mb
                new FileExtensionRule
            ]
        ];
    }

    public function messages()
    {
        return [
            'file.required' => __('message.file.required'),
            'file.max' => __('message.file.max'),
        ];
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FileUploadRequest;
use \App\Models\File;

class FilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $files = File::where('user_id', auth()->user()->id)->orderBy('id', 'DESC')->get();
        
        return view('files.index', compact('files'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('files.add');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FileUploadRequest $request)
    {
        if($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            
            $path = \Storage::put('files', $file);
            
            
            File::create([
                'file_name' => $filename,
                'user_id' => auth()->user()->id,
                'extension' => $extension,
                'path' => $path,
                'token' => uniqid() . time(),
            ]);
            
            return redirect()->route('files.index')->with('message', __('message.file.add'));
        }
        
        return redirect()->back();
    }
    
    /**
     * Display the specified resource.
     *
     * @param File $file
     * @return \Illuminate\View\View
     */
    public function show(File $file)
    {
        if ($file) {
            return view('files.show', compact('file'));
        }
        
        abort(404);
    }
    
    /**
     * Download the specified resource.
     *
     * @param File $file
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download(File $file)
    {
        if ($file) {
            $file->increment('total_download');
            
            return response()->download(storage_path('app/public/' . $file->path), $file->file_name);
        }
        
        abort(404);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    
    public function destroy($id)
    {
        $file = File::where([
            'id' => $id,
            'user_id' => auth()->user()->id
        ])->first();
        
        if ($file) {
            \Storage::delete($file->path);
            $file->delete();
            
            return redirect()->route('files.index')->with('message', __('message.file.delete'));
        }
        
        return redirect()->route('files.index')->with('error', __('message.error'));
    }
}
